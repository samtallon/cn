﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.IO;

public class GameUtility : MonoBehaviour {

	public static string[] LoadTextFromFileDynamic(string path)
	{
		if (!File.Exists (path)) {
			Debug.Log ("No such file");
			return null;
		}

		StreamReader file = new StreamReader (path);
		string str = file.ReadToEnd ();
		file.Close ();

		return str.Split ('\n');
	}

	public static string[] LoadTextFromFile(string name) 
	{
		TextAsset ta = (TextAsset)Resources.Load (name) as TextAsset;

		string str = ta.text;

		Resources.UnloadAsset (ta);

		return str.Split ('\n');
	}

	public static string[] LoadTextFromFile(TextAsset ta) 
	{
		return ta.text.Split ('\n');
	}

	public static string[] LoadTextFromFileUsingSeparator(TextAsset ta, char sep) 
	{
		return ta.text.Split (sep);
	}

	public static string[] LoadTextFromFileUsingSeparator(string name, char sep) 
	{
		TextAsset ta = (TextAsset)Resources.Load (name) as TextAsset;

		string str = ta.text;

		Resources.UnloadAsset (ta);

		return str.Split (sep);
	}

	public static void RewriteLineInTextFile(string fileName, int lineNum, string line)
	{
		string[] lines = LoadTextFromFileDynamic (fileName);

		if (lineNum >= lines.Length) {
			Debug.Log ("No such line number in file");

			return;
		}

		lines [lineNum] = line;

		string readyString = "";
		foreach (string s in lines)
			readyString += s+'\n';
		readyString = readyString.Remove (readyString.Length - 1);

		StreamWriter writer = new StreamWriter(fileName, false);
		writer.Write (readyString);
		writer.Close();
	}

    public static void AppendLineInTextFile(string fileName, string line)
    {
        StreamWriter writer = new StreamWriter(fileName, true);
        writer.WriteLine();
        writer.Write(line);
        writer.Close();
    }

    public static void CopyTxtFile(string source, string dest)
	{
		StreamReader reader = new StreamReader (source);
		string fileContent = reader.ReadToEnd ();
		reader.Close ();

		StreamWriter writer = new StreamWriter(dest, false);
		writer.Write (fileContent);
		writer.Close();
	}

	public static void DestroyChildren(GameObject go)
	{

		List<GameObject> children = new List<GameObject>();
		foreach (Transform tran in go.transform)
		{      
			children.Add(tran.gameObject); 
		}
		children.ForEach(child => GameObject.Destroy(child));  
	}

	public static Vector3 ScreenTouchHitCollider(Collider col)
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		if (col.Raycast (ray, out hit, Mathf.Infinity))
			return hit.point;

		return Vector3.zero;
	}

    public static bool ScreenTouchHitUI(GameObject uiObj)
    {
        PointerEventData cursor = new PointerEventData(EventSystem.current);                            
        cursor.position = Input.mousePosition;
        List<RaycastResult> objectsHit = new List<RaycastResult>();
        EventSystem.current.RaycastAll(cursor, objectsHit);

        int i;
        for (i = 0; i != objectsHit.Count; i++) {
            GameObject obj = objectsHit[i].gameObject;
            if (obj == uiObj)
                return true;
        }

        return false;
    }

    public static Vector3 Vector3HitCollider(Collider col, Vector3 vec)
	{
		Ray ray = Camera.main.ScreenPointToRay (vec);
		RaycastHit hit;

		if (col.Raycast (ray, out hit, Mathf.Infinity))
			return hit.point;

		return Vector3.zero;
	}

	public static Vector3 PointHitCollider(Vector3 v3, Collider col)
	{
		Ray ray = Camera.main.ScreenPointToRay (v3);
		RaycastHit hit;

		if (col.Raycast (ray, out hit, Mathf.Infinity))
			return hit.point;

		return Vector3.zero;
	}

    /*	public static bool GetImageSize(Texture2D asset, out int width, out int height) {
            if (asset != null) {
                string assetPath = AssetDatabase.GetAssetPath(asset);
                TextureImporter importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;

                if (importer != null) {
                    object[] args = new object[2] { 0, 0 };
                    MethodInfo mi = typeof(TextureImporter).GetMethod("GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance);
                    mi.Invoke(importer, args);

                    width = (int)args[0];
                    height = (int)args[1];

                    return true;
                }
            }

            height = width = 0;
            return false;
        }*/

    /*public void Swipe()
    {
        if (Input.GetMouseButtonDown(0))
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        if (Input.GetMouseButtonUp(0))
        {
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            currentSwipe.Normalize();

            if ((currentSwipe.y > 0) && (currentSwipe.x > -swipeThreshold) && (currentSwipe.x < swipeThreshold))
            {
                //Debug.Log("up swipe");
            }
            if ((currentSwipe.y < 0) && (currentSwipe.x > -swipeThreshold) && (currentSwipe.x < swipeThreshold))
            {
                //LandTet();
            }
            if ((currentSwipe.x < 0) && (currentSwipe.y > -swipeThreshold) && (currentSwipe.y < swipeThreshold))
            {
                //Debug.Log("left swipe");
            }
            if ((currentSwipe.x > 0) && (currentSwipe.y > -swipeThreshold) && (currentSwipe.y < swipeThreshold))
            {
                //Debug.Log("right swipe");
            }
        }
    }*/

}
