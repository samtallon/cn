﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingBall : MonoBehaviour
{
    public float destroyTimer;

    private Rigidbody rb;
    private bool isActed = false;

    private ShootingRangeController boss;

    // Start is called before the first frame update
    void Start()
    {
        boss = GameObject.FindGameObjectWithTag("boss").GetComponent<ShootingRangeController>();

        rb = GetComponent<Rigidbody>();
    }

    public void Shoot(Vector3 dir)
    {
        rb.useGravity = true;
        rb.AddForce(dir * boss.ballSpeedMult, ForceMode.Impulse);

        isActed = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isActed)
            return;

        destroyTimer -= Time.deltaTime;

        if (destroyTimer <= 0) {
            boss.EndBall();
            Destroy(gameObject);
        }
    }
}